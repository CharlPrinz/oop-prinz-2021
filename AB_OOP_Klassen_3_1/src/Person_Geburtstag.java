
public class Person_Geburtstag {
	
	public static void main(String[] args) {
		Datum b1 = new Datum(12, 6, 1990);
		Datum b2 = new Datum(25, 3, 1995);
		Datum b3 = new Datum(15, 12, 1990);
		Datum b4 = new Datum(19,1,2014);
		Person p1 = new Person("Charlotte", b1);
		Person p2 = new Person("Leon", b2);
		Person p3 = new Person("Lisa", b3);
		Person p4 = new Person("Nahuel", b4);

		System.out.println("Anzahl der Personen: "+ p1.getAnzahl());
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		System.out.println(p4);
	}
}
