
public class Person {
	
	private Datum birth;
	private String name;
	private static int anzahl =0; 
	private int value =0; 

	public Person() {
		this.name = "Unbekannt";
		this.birth = null;
		value = this.anzahl ++ ;	
	}
	public Person(String name, Datum birth) {
		setName(name);
		this.birth = birth;
		value = this.anzahl++ ;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Datum getBirth() {
		return birth;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	@Override
	public String toString() {
		return 	this.name + " hat am " + this.birth + " Geburtstag! Personennummer: " + this.value;
	}
}
