
public class KomplexZahl_Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		KomplexeZahl z1 = new KomplexeZahl(2, 5);
		KomplexeZahl z2 = new KomplexeZahl(2, 6);

		KomplexeZahl z3 = KomplexeZahl.mult(z1, z2);
		KomplexeZahl z4 = z1.mult2(z2);

		System.out.println("Die erste komplexe Zahl ist: " + z1);
		System.out.println("Die zweite komplexe Zahl ist: " + z2);
		System.out.println("Das Ergebnis der Multiplikation beider Zahlen ist:" + z3);
		System.out.println("Das Ergebnis der Multiplikation beider Zahlen ist:" + z4);

		if (z1.equals(z2))
			System.out.println("Die Zahlen sind gleich");
		else
			System.out.println("Die Zahlen sind ungleich");
	}
}
