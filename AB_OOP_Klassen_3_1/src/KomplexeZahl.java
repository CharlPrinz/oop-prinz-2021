
public class KomplexeZahl {

	private double real;
	private double imag;

	public KomplexeZahl() {
		this.real = 0.0;
		this.imag = 0.0;
	}
	public KomplexeZahl(double real, double imag) {
		setReal(real);
		setImag(imag);
	}
	public double getReal() {
		return real;
	}
	public double getImag() {
		return imag;
	}
	public double setReal(double real) {
		return this.real = real;
	}
	public double setImag(double imag) {
		return this.imag = imag;
	}
	public String toString () {
		return String.format("%.2f%+.2fj %n", this.real, this.imag);
	}
	public boolean equals(Object obj) {

		if (obj instanceof KomplexeZahl) {
			KomplexeZahl z = (KomplexeZahl) obj;
			if (this.real == (z.getReal()) && this.imag == (z.getImag()))
				return true;
			else
				return false;
		} else
			return false;
	}

	public static KomplexeZahl mult(KomplexeZahl z1, KomplexeZahl z2)
	{
		double realval = z1.getReal()*z2.getReal() - z1.getImag()*z2.getImag();
		double imagval = z1.getReal()*z2.getImag() + z1.getImag()*z2.getReal();
		return new KomplexeZahl(realval,imagval);
	}
	public KomplexeZahl mult2(Object obj)
	{	KomplexeZahl z = (KomplexeZahl) obj;
		double realval = this.real * z.getReal() - this.imag * z.getImag();
		double imagval = this.real * z.getImag() + this.imag * z.getReal();
		return new KomplexeZahl(realval,imagval);
	}
}
		
