
public class Buch implements Comparable<Buch> {
	
	private String autor; 
	private String titel;
	private String isbn;
	
	
	public Buch(String autor, String titel, String isbn) {
		this.autor = autor; 
		this.titel = titel;
		this.isbn = isbn;
	}
	
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}
	
	public void setTitel(String titel) {
		this.titel = titel;
	}
	
	public String getIsbn() {
		return isbn;
	}
	
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	} 
	
	
	public boolean equals(Buch b){
	if(this.isbn.equals(b.getIsbn())== true)
		return true;
	else 
		return false; 
	}
	
	@Override 
	public int compareTo(Buch b) {
		return this.isbn.compareTo(b.getIsbn());
	}

	@Override
	public String toString() {
		return "Buchautor: " + autor + ", Titel: " + titel + ", isbn: " + isbn;
	}

	
}
	

	
	
	
	

