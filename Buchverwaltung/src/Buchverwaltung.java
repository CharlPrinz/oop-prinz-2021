import java.util.*;

public class Buchverwaltung {

	static void menue() {
		System.out.println("\n ***** Buchverwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) löschen");
		System.out.println(" 4) Die größte ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Buch> buchliste = new ArrayList<Buch>();
		buchliste.add(new Buch("max", "oop", "12345"));
		buchliste.add(new Buch("tom", "java", "56789"));
		buchliste.add(new Buch("fritz", "DB", "09876"));
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Autor: ");
				String autor = myScanner.next();
				System.out.println("Titel: ");
				String titel = myScanner.next();
				System.out.println("ISBN: ");
				String isbn = myScanner.next();
				Buch b = new Buch(autor, titel, isbn);
				buchliste.add(b);

				break;
			case '2':
				System.out.println("ISBN: ");
				String isbn2 = myScanner.next();

				boolean gefunden = false;

				for (Buch buch : buchliste) {
					if (buch.getIsbn().equals(isbn2))
//			if((buch.getIsbn().compareTo(isbn2))==0)
					{
						System.out.println(buch.getTitel());
						gefunden = true;
					}
				}
				if (gefunden == false)
					System.out.println("Kein Buch zu der ISBN gefunden");
				break;
			case '3':
				System.out.println("ISBN des zu löschenden Buches:");
				String isbnd = myScanner.next();
				boolean zumlöschen = false;
				for (Buch b1 : buchliste) {
					if (b1.getIsbn().equals(isbnd)) {
						buchliste.remove(b1);
						zumlöschen = true;
						break;
					}
				}
				if (zumlöschen == false)
					System.out.println(
							"Das Buch mit der eingegebn ISBN ist nicht vorhanden, und kann nicht gelöscht werden.");
				break;
			case '4':
				Collections.sort(buchliste);
				System.out.println(buchliste.get(buchliste.size() - 1));

				break;
			case '5':
				System.out.println(buchliste);

				break;
			case '6':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

}
