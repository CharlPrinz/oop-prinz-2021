
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buchverwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) löschen");
		System.out.println(" 4) Die größte ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {

		ArrayList<Buch> buchliste = new ArrayList<Buch>();
//		LinkedList<Buch> buchliste = new LinkedList<Buch>();
		buchliste.add(new Buch("max", "oop", "12345"));
		buchliste.add(new Buch("tom", "java", "56789"));
		buchliste.add(new Buch("fritz", "DB", "09876"));
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Autor: ");
				String autor = myScanner.next();
				System.out.println("Titel: ");
				String titel = myScanner.next();
				System.out.println("ISBN: ");
				String isbn = myScanner.next();
				Buch b = new Buch(autor, titel, isbn);
				buchliste.add(b);

				break;
			case '2':
				System.out.println("ISBN: ");
				String isbn2 = myScanner.next();
				System.out.println(findeBuch(buchliste, isbn2));

				break;
			case '3':
				System.out.println("ISBN des zu löschenden Buches:");
				String isbnd = myScanner.next();
				loescheBuch(buchliste, isbnd);

				break;
			case '4':
				System.out.println(ermitteleGroessteISBN(buchliste));
				break;
			case '5':
				System.out.println(buchliste);

				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			}

		} while (wahl != 9);
	}

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {

		for (Buch buch : buchliste) {
			if (buch.getIsbn().equals(isbn))
				return buch;
		}
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, String isbnd) {
		for (Buch b1 : buchliste) {
			if (b1.getIsbn().equals(isbnd)) {
				buchliste.remove(b1);
				System.out.println("Buch mit der ISBN: " + isbnd + " gelöscht");
				return true;
			} else
				System.out.println(
						"Das Buch mit der eingegebn ISBN ist nicht vorhanden, und kann nicht gelöscht werden.");

		}
		return false;

	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		Collections.sort(buchliste);

		return buchliste.get(buchliste.size() - 1).getIsbn();
	}
}
