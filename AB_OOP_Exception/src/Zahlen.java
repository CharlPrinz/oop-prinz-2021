
public class Zahlen {

	public static void main(String[] args) {

		try {
			System.out.println("Ergebnis der Division ist " + division(4,0));

		} catch (ArithmeticException ex) {
			System.out.println("Division durch Null, Berechnung nicht möglich!");
		}
	}

	public static double division(int zahl1, int zahl2) throws ArithmeticException {

		if (zahl2 == 0) {
			throw new ArithmeticException();
		}
		double ergebnis = zahl1 / zahl2;
		return ergebnis;
	}

}
