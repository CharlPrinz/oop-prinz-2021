package myUtil;

public class Main_Aufgabe6_3 {

	public static void main(String[] args) {

		MyIntArray a1 = new MyIntArray(3);
		a1.increase(4);
		try {
			a1.set(4, 2);
			System.out.println(a1);
		} catch (MyIndexException ex) {
			a1.increase(ex.getWrongIndex());
			System.out.println(a1);

			System.out.println(ex.getMessage());
			System.out.println(ex.getWrongIndex());
		}
	}

}
