
public abstract class falschesDatumException extends Exception {

	private int wert;

	public falschesDatumException(String text, int wert) {
		super(text);
		this.wert = wert;
	}

	public int getWert() {
		return this.wert;
	}

}
