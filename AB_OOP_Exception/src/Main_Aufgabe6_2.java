
public class Main_Aufgabe6_2 {

	public static void main(String[] args) {

		Datum d1;

		try {
			d1 = new Datum(31, 10, 2101);
			System.out.println(d1);

		} catch (falscherTagException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getWert());
		} catch (falscherMonatException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getWert());
		} catch (falschesJahrException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getWert());
		}
	}
}
