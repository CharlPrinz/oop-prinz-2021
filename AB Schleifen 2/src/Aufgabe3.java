import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {

		// Quersumme
		int zahl; 
		int summe = 0;
		
	  	Scanner myScanner = new Scanner(System.in);
	  	System.out.println("Geben Sie eine Zahl ein: ");
	  	zahl = myScanner.nextInt();
	  	while (0 != zahl)
	  	{
	  	summe = summe + (zahl % 10);
	  	zahl = zahl / 10;
	  	}
	  	System.out.printf ("%d ", summe );
	}
}


