
public class Kreis extends GeoObject {

	private double radius;

	public Kreis(double x, double y, double radius) {
		super(x, y);
		this.radius = radius;
	}

	@Override
	public double determineArea() {
		return radius * 3.1415;
	}

	@Override
	public String toString() {
		return "Radius:" + radius + ", determineArea:" + determineArea();
	}

}
