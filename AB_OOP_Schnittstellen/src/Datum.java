
public class Datum implements Comparable<Datum>  {
	private int tag;
	private int monat;
	private int jahr;
	
	
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) {
		this.tag = tag;
		this.monat = monat;
		this.jahr = jahr;
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	public static int berechneQuartal(Datum d){
		return d.monat/3 +1;
	}
	
	@Override 
	public int compareTo(Datum d) {

		if (this.jahr > d.getJahr())
			return 1;
		
		if (this.jahr < d.getJahr())
			return -1;
		
		else 
			if (this.monat > d.getMonat())
			return 1;
		
		if (this.monat < d.getMonat())
			return -1;
		
		else if (this.tag > d.getTag())
			return 1;
		
		if (this.tag < d.getTag())
			return -1; 
		
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag+"."+this.monat+"."+this.jahr;
	}
		

}