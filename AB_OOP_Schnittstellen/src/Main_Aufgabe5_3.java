
public class Main_Aufgabe5_3 {

	public static void main(String[] args) {

		Stack st1 = new Stack(20);

		st1.push(2);
		st1.push(3);
		st1.stackausgabe();
		st1.pop();
		st1.stackausgabe();
		st1.pop();
		st1.stackausgabe();
		st1.pop();
		st1.stackausgabe();
		st1.pop();
		st1.stackausgabe();
		st1.push(3);
		st1.push(5);
		st1.push(4);
		st1.push(3);
		st1.push(5);
		st1.push(4);
		st1.stackausgabe();
		st1.pop();
		st1.stackausgabe();
		st1.pop();
		st1.pop();
		st1.pop();
		st1.stackausgabe();
	}

}
