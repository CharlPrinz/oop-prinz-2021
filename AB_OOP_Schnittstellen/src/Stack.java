
public class Stack implements IntStack {

	private int[] zahlenliste;
	private int cpos = 0;

	public Stack(int zahlenliste) {
		this.zahlenliste = new int[100];
	}

	@Override
	public void push(int i) {
		zahlenliste[cpos] = i;
		cpos++;
	}

	public int pop() {
		if (cpos > 0) {
			cpos--;
			return zahlenliste[cpos];
		}
		return cpos;
	}

	public void stackausgabe() {
		System.out.print("[");
		for (int j = 0; j < this.cpos; j++) {
			if (j == (this.cpos - 1))
				System.out.print(zahlenliste[j]);
			else
				System.out.print(zahlenliste[j] + ",");
		}
		System.out.println("]");
	}
}
