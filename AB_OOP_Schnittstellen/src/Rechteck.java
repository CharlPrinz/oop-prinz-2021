
public class Rechteck extends GeoObject {

	private int hoehe;
	private int breite;

	public Rechteck(double x, double y, int hoehe, int breite) {
		super(x, y);
		this.hoehe = hoehe;
		this.breite = breite;
	}

	@Override
	public double determineArea() {
		return hoehe * breite;
	}

	@Override
	public String toString() {
		return "Hoehe:" + hoehe + ", Breite:" + breite + ", determineArea:" + determineArea();
	}

}
