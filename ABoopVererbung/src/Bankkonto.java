
import java.util.Scanner;

public class Bankkonto {

	private String inhaber;
	private String kontNr;
	private double guthaben;
	Scanner myScanner = new Scanner(System.in);

	public Bankkonto(String inhaber, String kontNr, double guthaben) {
		setInhaber(inhaber);
		setKontNr(kontNr);
		setGuthaben(guthaben);
	}

	public String getInhaber() {
		return inhaber;
	}

	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}

	public String getKontNr() {
		return kontNr;
	}

	public void setKontNr(String kontNr) {
		this.kontNr = kontNr;
	}

	public double getGuthaben() {
		return guthaben;
	}

	public void setGuthaben(double guthaben) {
		this.guthaben = guthaben;
	}

	public void einzahlen() {
		
		System.out.println("Wieivel Geld soll eingezahlt werden:");
		double betrag = myScanner.nextDouble(); 
		guthaben += betrag;
	}

	public double auszahlen() {
		System.out.println("Wieviel Geld soll abgehoben werden:");
		double betrag = myScanner.nextDouble();
		guthaben -= betrag;
		System.out.println("Ihnen wurden " + betrag + "€ ausgezahlt");
		return betrag;
	}

	@Override
	public String toString() {
		return  String.format ("Kontoinhaber: %s \nKontonummer: %s \nGuthaben: %.2f€", this.inhaber , this.kontNr , this.guthaben);
	}

	
}

