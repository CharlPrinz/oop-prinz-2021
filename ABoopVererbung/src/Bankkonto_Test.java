import java.util.Scanner;

public class Bankkonto_Test {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		Bankkonto bk1 = new Bankkonto("Anna", "123456789", 120);
		Dispokonto dk1 = new Dispokonto("Max", "123456789", 60, 100);

		Bankkonto[] kliste = new Bankkonto[4];

		kliste[0] = bk1;
		kliste[1] = new Bankkonto("Peter", "524896348", 5692);
		kliste[2] = dk1;
		kliste[3] = new Dispokonto("Paula", "1568453845", 256, 1000);

		for (int i = 0; i < kliste.length; i++)
			System.out.println(kliste[i]);

		/*
		 * while (true) { System.out.println(dk1); System.out.println(bk1);
		 * System.out.println("Geld einzahlen (1) ");
		 * System.out.println("Geld auszahlen (2) ");
		 * 
		 * int z = myScanner.nextInt();
		 * 
		 * if (z == 1) { dk1.einzahlen(); bk1.einzahlen(); System.out.println(dk1);
		 * System.out.println(bk1);
		 * 
		 * } else if (z == 2) { dk1.auszahlen(); // bk1.auszahlen(); } }
		 */
	}
}
