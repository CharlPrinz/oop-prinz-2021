
public class Dispokonto extends Bankkonto {

	double dispokredit;

	public Dispokonto(String inhaber, String kontNr, double guthaben, double dispokredit) {
		super(inhaber, kontNr, guthaben);
		setDispokredit(dispokredit);
	}

	public double getDispokredit() {
		return dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}

	public double auszahlen() {
		System.out.print("Wieviel Geld soll abgehoben werden:");
		double betrag = myScanner.nextDouble();
		if ((this.getGuthaben() + dispokredit) >= betrag) 
		{
			this.setGuthaben(this.getGuthaben() - betrag);
		} else if ((this.getGuthaben() + dispokredit) < betrag) 
		{
			System.out.println("Nicht genügend Guthaben verfügbar! Es wird der Betrag bis zum Dispokredit ausgezahlt");
			betrag = (this.getGuthaben() + dispokredit);
			this.setGuthaben(this.getGuthaben() - betrag);
			System.out.println("Es können maximal: " + betrag + "€ ausgezahlt werden!");
		}
		return betrag;
	}

	@Override
	public String toString() {
		return String.format("%s\nDispokredit: %.2f€", super.toString(), this.dispokredit);
	}
}
