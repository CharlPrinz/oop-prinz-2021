
public class Datum {

	private int day;
	private static int month;
	private int year;

	public Datum() {
		this.day = 1;
		this.month = 1;
		this.year = 1970;
	}

	public Datum(int day, int month, int year) {
		setDay(day);
		setMonth(month);
		setYear(year);
	}

	public int getDay() {
		return day;
	}

	public int setDay(int day) {
		if (day <= 31 && day > 0)
			return this.day = day;
		else
			return 0;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public int setMonth(int month) {
		if (month <= 12 && month > 0)
			return this.month = month;
		else
			return 0;
	}

	public int setYear(int year) {
		return this.year = year;
	}
	
@Override
	public String toString() {
		return this.day + "." + this.month + "." + this.year;
	}


}
