
public class Feiertag extends Datum {

	private String feiertagname;

	public Feiertag() {
		super();
		this.feiertagname = "---";
	}

	public Feiertag(int day, int month, int year, String feiertagname) {
		super(day, month, year);
		setFeiertagname(feiertagname);
	}

	public String getFeiertagname() {
		return feiertagname;
	}

	public void setFeiertagname(String feiertagname) {
		this.feiertagname = feiertagname;
	}

	@Override
	public String toString() {
		return super.toString() + " (" + this.feiertagname + ")";
	}
}
