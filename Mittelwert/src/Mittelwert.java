import java.util.Scanner;

public class Mittelwert {

	
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
//      double x = 2.0;
	  double x = doubleEinlesen("Geben Sie bitte eine Zahl ein: ");
      double y = doubleEinlesen("Geben Sie bitte eine Zahl ein: ");
      double m;
      
  
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
     // m = (x + y) / 2.0;
      
      m = mittelwertBerechnen (x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
//      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      printResult(x,y,m);
   }
   
   public static double mittelwertBerechnen (double z1, double z2)
   
   {
	   double ergebnis;
	   ergebnis = (z1+z2)/2.0;
	   return ergebnis; 
   }
  
   
   public static double doubleEinlesen(String text) {
  	  Scanner myScanner = new Scanner(System.in);
  	  System.out.println(text);
  	  double ergebnis = myScanner.nextDouble();
  	  return ergebnis;
  	  
    }
   
   public static void printResult(double a, double b, double m) {
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", a, b, m);
	   
   }
   

}




