
import java.util.Scanner;

public class LS_Fahrkartenautomat_3_7 {

	public static void main(String[] args) {

		while (true) {

			double preis = fahrkartenbestellungErfassen();
			double zuZahlen = fahrkartenBezahlen(preis);
			fahrkartenAusgeben();
			rueckgeldAusgeben(preis, zuZahlen);

		}
	}

	// Geldeinwurf
	// -----------

	public static double fahrkartenbestellungErfassen() {

		double preis = 0;
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		int auswahl;
		int m = 0;
		double ticketpreis[] = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90, 5.00 };

		String bezeichnung[] = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tagesticket Berlin AB", "Tagesticket Berlin BC",
				"Tagesticket Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC", "Berlin Spezial" };

		System.out.println("Fahrkarten: ");

		for (int i = 0; i < bezeichnung.length; i++)

		{
			if (m < bezeichnung[i].length()) {
				m = bezeichnung[i].length();
			}
		}
		for (int i = 0; i < bezeichnung.length; i++) {
			System.out.printf("%12d    ", i + 1);
			System.out.printf("%s", bezeichnung[i]);
			for (int j = bezeichnung[i].length(); j < m + 3; j++) {
				System.out.print(" ");
			}

			System.out.printf("%5.2f", ticketpreis[i]);
			System.out.printf("€");
			System.out.println();
		}
		do {
			System.out.println();
			System.out.print("Ihre Wahl: ");
			auswahl = tastatur.nextInt();

			if (auswahl < 1 || auswahl > bezeichnung.length) {
				System.out.println("Falsche Eingabe bitte wählen Sie ihre Wunschfahrkarte");
			}

		} while (1 > auswahl || auswahl > bezeichnung.length);

		System.out.print("Anzahl der Tickets: ");
		int anzahlTicket = tastatur.nextInt();
		zuZahlenderBetrag = ticketpreis[auswahl - 1];
		preis = (zuZahlenderBetrag * anzahlTicket) * 100;
		return preis;
	}

	// Fahrscheinausgabe
	// -----------------

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warten(1);
	}

	public static void warten(int millisekunden) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// Rückgeldberechnung und -Ausgabe
	// -------------------------------
	public static void muenzeAusgabe(int betrag, String einheit) {
		System.out.printf(" %d  %s%n", betrag, einheit);
	}

	public static double fahrkartenBezahlen(double preis) {
		double eingezahlterGesamtbetrag = 0.00;

		while (eingezahlterGesamtbetrag < preis) {
			Scanner tastatur = new Scanner(System.in);
			System.out.printf("Noch zu zahlen:  %.2f\n", ((preis) - eingezahlterGesamtbetrag) / 100);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = 100 * tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static double rueckgeldAusgeben(double preis, double zuZahlen) {
		double rückgabebetrag = ((zuZahlen - preis));

		if (rückgabebetrag > 0.00) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag / 100);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 200) // 2 EURO-Münzen
			{
				muenzeAusgabe(2, "Euro");
				rückgabebetrag -= 200;

			}
			while (rückgabebetrag >= 100) // 1 EURO-Münzen
			{
				muenzeAusgabe(1, "Euro");
				rückgabebetrag -= 100;
			}
			while (rückgabebetrag >= 50) // 50 CENT-Münzen
			{
				muenzeAusgabe(50, "Cent");
				rückgabebetrag -= 50;
			}
			while (rückgabebetrag >= 20) // 20 CENT-Münzen
			{
				muenzeAusgabe(20, "Cent");
				rückgabebetrag -= 20;
			}
			while (rückgabebetrag >= 10) // 10 CENT-Münzen
			{
				muenzeAusgabe(10, "Cent");
				rückgabebetrag -= 10;
			}
			while (rückgabebetrag >= 5)// 5 CENT-Münzen
			{
				muenzeAusgabe(5, "Cent");
				rückgabebetrag -= 5;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		System.out.println();
		System.out.println();
		System.out.println();
		return rückgabebetrag;
	}
}
