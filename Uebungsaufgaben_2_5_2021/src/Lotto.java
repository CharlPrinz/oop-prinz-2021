import java.util.Scanner;

public class Lotto {
	public static void main(String[] args) {
		int[] lottoziehungen = { 3, 7, 12, 18, 37, 42 };
		Scanner tastatur = new Scanner(System.in);
		printArray(lottoziehungen);
		while (true) {
			System.out.print("Zahl: ");
			int gesucht = tastatur.nextInt();
			lottozahlUeberpruefen(lottoziehungen, gesucht);
		}
	}

	public static void lottozahlUeberpruefen(int[] zliste, int zahl) {
		boolean enthalten = false;
		for (int i : zliste) {
			if (i == zahl) {
				enthalten = true;
				break;
			}
		}
		if (enthalten == true)
			System.out.printf("%d enthalten.%n", zahl);

		else
			System.out.printf("%d nicht enthalten.%n", zahl);
	}

	public static void printArray(int[] zliste) {
		System.out.print("[ ");

		for (int i = 0; i < zliste.length; i++)
			System.out.print(zliste[i] + " ");

		System.out.println("]");
		System.out.println();
	}
}

