
import java.util.Scanner;

public class Aufgabenblatt_Schleifen_1_2c {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Zahl: ");
		int n = myScanner.nextInt();
		int summe = 0;

		for (int i = 0; i <= n; i++) // ++i oder i= i+1
		{
			if (i % 2 != 0) {
				summe = summe + i;
			}
		}
		System.out.print("Summe für C beträgt: " + summe);
	}
}
