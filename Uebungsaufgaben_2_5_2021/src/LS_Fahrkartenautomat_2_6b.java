import java.util.Scanner;

public class LS_Fahrkartenautomat_2_6b {

	public static void main(String[] args) {

		while (true) {

			Scanner tastatur = new Scanner(System.in);

			double preis = fahrkartenbestellungErfassen();
			double zuZahlen = fahrkartenBezahlen(preis);
			fahrkartenAusgeben();
			rueckgeldAusgeben(preis, zuZahlen);

		}
	}

	// Geldeinwurf
	// -----------

	public static double fahrkartenbestellungErfassen() {

		double preis = 0;
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;

		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
		System.out.println(" ");
		System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println(" ");
		System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println(" ");
		System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		System.out.println(" ");
		int auswahl;

		do {
			System.out.print("Ihre Wahl: ");
			auswahl = tastatur.nextInt();

			if (auswahl == 1)

			{
				zuZahlenderBetrag = 2.90;
				System.out.println("Ihre Wahl : 1");
			}

			if (auswahl == 2) {
				zuZahlenderBetrag = 8.60;
				System.out.println("Ihre Wahl : 2");
			}

			if (auswahl == 3) {
				zuZahlenderBetrag = 23.50;
				System.out.println("Ihre Wahl : 3");
			}
			if (1 > auswahl || auswahl > 3) {
				System.out.println("Falsche Eingabe bitte wählen Sie ihre Wunschfahrkarte");
			}
		}

		while (1 > auswahl || auswahl > 3);

		System.out.print("Anzahl der Tickets: ");
		int anzahlTicket = tastatur.nextInt();

		preis = (zuZahlenderBetrag * anzahlTicket) * 100;
		return preis;
	}
	// Fahrscheinausgabe
	// -----------------

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warten(1);
	}

	public static void warten(int millisekunden) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// Rückgeldberechnung und -Ausgabe
	// -------------------------------
	public static void muenzeAusgabe(int betrag, String einheit) {
		System.out.printf(" %d  %s%n", betrag, einheit);
	}

	public static double fahrkartenBezahlen(double preis) {
		double eingezahlterGesamtbetrag = 0.00;

		while (eingezahlterGesamtbetrag < preis) {
			Scanner tastatur = new Scanner(System.in);
			System.out.printf("Noch zu zahlen:  %.2f\n", ((preis) - eingezahlterGesamtbetrag) / 100);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = 100 * tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static double rueckgeldAusgeben(double preis, double zuZahlen) {
		double rückgabebetrag = ((zuZahlen - preis));

		if (rückgabebetrag > 0.00) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag / 100);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 200) // 2 EURO-Münzen
			{
				muenzeAusgabe(2, "Euro");

				rückgabebetrag -= 200;
				System.out.printf(" %d  %s%n", rückgabebetrag);
			}
			while (rückgabebetrag >= 100) // 1 EURO-Münzen
			{
				muenzeAusgabe(1, "Euro");
				rückgabebetrag -= 100;
			}
			while (rückgabebetrag >= 50) // 50 CENT-Münzen
			{
				muenzeAusgabe(50, "Cent");
				rückgabebetrag -= 50;
			}
			while (rückgabebetrag >= 20) // 20 CENT-Münzen
			{
				muenzeAusgabe(20, "Cent");
				rückgabebetrag -= 20;
			}
			while (rückgabebetrag >= 10) // 10 CENT-Münzen
			{
				muenzeAusgabe(10, "Cent");
				rückgabebetrag -= 10;
			}
			while (rückgabebetrag >= 5)// 5 CENT-Münzen
			{
				muenzeAusgabe(5, "Cent");
				rückgabebetrag -= 5;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		return rückgabebetrag;
	}
}
