
import java.util.Scanner;

public class Aufgabenblatt_Schleifen_1_6 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Anzahl der Stufen: ");
		int n = myScanner.nextInt();

		for (int i = 0; i <= n - 1; i++) {
			for (int j = 0; j <= i; j++) // Wenn nur eine Zeile, kann geschweifte Klammer weggelassen werden!
				System.out.print("*");

			System.out.println();
		}
	}
}
