import java.util.Scanner;

public class Aufgabenblatt_Schleifen_2_1a {

	public static void main(String[] args) {

		int n;
		int x = 1;

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die Zahl ein, bis zu welcher gezaehlt werden soll: ");
		n = myScanner.nextInt();

		while (x <= n) {
			if (x < n) {
				System.out.print(x + ",");
				x++;
			}
			if (x == n) {
				System.out.print(x);
				x++;
			}
		}
	}
}
